export const environment = {
  production: true,
  apiUrl: '//localhost/aguagente/api/public',
  months_ahead: [{id:1}, {id:3}, {id:6}, {id:12}],
  payment_types: [{name: 'OXXO'}, {name: 'SPEI'}],
};
