// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  apiUrl: '//localhost/GITBW/api/public',
  months_ahead: [
    {
      id:1
    }, 
    {
      id:3
    }, 
    {
      id:6
    }, 
    {
      id:12
    }
  ],
  payment_types: [
    {
      name: 'OXXO'
    }, 
    {
      name: 'SPEI'
    }
  ],
  // apiUrl: '//panelaguagente.xyz/api/public'
};
