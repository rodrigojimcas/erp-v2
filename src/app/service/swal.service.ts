
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import Swal from 'sweetalert2';

@Injectable({
    providedIn: 'root'
})

export class SwalService {
  
    public actionEvent = new BehaviorSubject<any>({});
    constructor() { }

    public actionConfirmation($title) {

        return Swal.fire({
            icon:'warning',
            title: $title,
            showCancelButton: true,
            allowOutsideClick: false,
            confirmButtonColor: '#128f76',
            confirmButtonText: 'Si',
            cancelButtonText: 'No',
            showLoaderOnConfirm: true
        })
    }

    public inputSwal($title, $text) {
        
        return Swal.fire({
            title: $title,
            text: $text,
            input: 'textarea',
            inputPlaceholder: 'Razones',
            inputAttributes: {
                autocapitalize: 'on'
            },
            showCancelButton:true,
            confirmButtonColor: '#128f76',
            confirmButtonText: 'Guardar',
            cancelButtonText: 'Cancelar',
        });
    }

    public error($text) {
        
        return Swal.fire({
            icon: 'error',
            title: 'Error',
            text: $text,
            confirmButtonColor: '#128f76',
            confirmButtonText: 'Confirmar',
            allowOutsideClick: false,
            onClose: () => {
                // call (event)DatatableService to broadcast the params
            }
        });
    }

    public success(params?) {

        return Swal.fire({
            icon: 'success',
            title: '!Exito!',
            confirmButtonColor: '#128f76',
            confirmButtonText: 'OK',
            allowOutsideClick: false,
            onClose: () => {
                // call (event)DatatableService to broadcast the params
            }
        });
    }

    public warning($title) {
        return Swal.fire({
            icon: 'warning',
            title: $title,
            confirmButtonColor: '#128f76',
            confirmButtonText: 'OK',
            allowOutsideClick: false,
            onClose: () => {
                // call (event)DatatableService to broadcast the params
            }
        })
    }
}