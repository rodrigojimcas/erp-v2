import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})

export class AuthorizationService {

  baseUrl = `${environment.apiUrl}`;
  cookieValue = 'NA';
  jwt = null;

  constructor(
    private http: HttpClient,
  ) {}

  login(credentials) {
    
    return this.http.post(`${this.baseUrl}/auth/login`, credentials,{withCredentials: true});

  }

  isAuthenticated() {
    let isAuthorize = false;
    if (window.localStorage.getItem('access_token')) {
      isAuthorize = true;
    }
    return isAuthorize;
  }

}