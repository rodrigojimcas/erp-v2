import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})

export class BadDebtsService {
    baseUrl = `${environment.apiUrl}`;
    constructor(private http: HttpClient) { }

    getBadDebtsClients(params?, stateSelect?) {
        if (!params) {
            const params = [];
        }
        return this.http.post(`${this.baseUrl}/bad-debts/getBadDebtsClients`, params);
    }

    moveToBadDebt(id?, params?) {
        return this.http.post(`${this.baseUrl}/bad-debts/moveBadDebt/${id}`, params);
    }

    restoreClient(id?, params?) {
        console.log(id);
        return this.http.post(`${this.baseUrl}/bad-debts/restoreClient/${id}`, params);
    }
}