import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})

export class ClientsService {
  baseUrl = `${environment.apiUrl}`;
  constructor(private http: HttpClient) { }

  getClients(params?, filters?) {
    if (!params) {
      const params = [];
    }
    if (filters) {
      params['status'] = filters['status'];
    }
    if (filters) {
      params['serial_number'] = filters['serial_number'];
    }
    if (filters) {
      params['sales_agent'] = filters['sales_agent'];
    }
    return this.http.post(`${this.baseUrl}/clients/get_clients`, params);
  }

  show(id) {
    return this.http.get(`${this.baseUrl}/clients/${id}`);
  }

  setStatus(id?, params?) {
    return this.http.post(`${this.baseUrl}/clients/${id}/setStatus`, params);
  }

  subscribeToPlan(id, params?) {
    return this.http.post(`${this.baseUrl}/clients/${id}/subscribeToPlan`, params);
  }

  subscribeToPlanOffline(id, params) {
    return this.http.post(`${this.baseUrl}/clients/${id}/subscribeToPlanOffline`, params);
  }

  makeSalesAgent(id?, params?) {
    return this.http.post(`${this.baseUrl}/clients/${id}/attachRole`, params);
  }

  update(id, params?) {
    return this.http.put(`${this.baseUrl}/clients/${id}`, params);
  }

  getHierarchicalTree(id) {
    return this.http.get(`${this.baseUrl}/clients/${id}/hierarchicalTree`);
  }
  
  getCards(params, filters?) {
    let id = 0;
    if (filters) {
      id = filters['id_clients'];
      console.log(id);
    }

    return this.http.post(`${this.baseUrl}/client/${id}/getCards`, params);
  }
}