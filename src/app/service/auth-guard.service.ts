import { Injectable } from '@angular/core';
import {Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate} from '@angular/router';
import { AuthorizationService } from './authorization.service';


@Injectable({
    providedIn: 'root'
  })
  export class AuthGuardService implements CanActivate {
    userData: any;
    isLoggedIn = false;

    constructor(
      private auth: AuthorizationService,
      private router: Router
    ) { }

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      if (this.auth.isAuthenticated()) {
        return true;
      } else {
        this.router.navigate(['authentication']);
      }
    }
    
    getLocalStorageUser() {
      this.userData = JSON.parse(localStorage.getItem('access_token'));
      if (this.userData) {
        this.isLoggedIn = true;
        return true;
      } else {
        this.isLoggedIn = false;
        return false;
      }
    }
  }