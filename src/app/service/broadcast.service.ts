import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

export interface Events {
  name: string;
  data: any;
}
@Injectable({
  providedIn: 'root'
})
export class BroadcastService {
  public events = new BehaviorSubject<Events>({name: '', data: {}});
  
  constructor() { }

  fire(data: Events) {
    this.events.next(data);
  }
}
