import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TicketsService {
  baseUrl = `${environment.apiUrl}`;
  constructor(private http: HttpClient) {}
  
  getTickets(params?, filters?){
    if (!params) {
      const params = [];
    }

    if (filters) {
      params['status'] = filters['status'];
    }

    return this.http.post(`${this.baseUrl}/tickets/get_tickets`, params);
 }

  getErrorCodes(params?, filters?) {
    if (!params) {
      const params = [];
    }
    return this.http.get(`${this.baseUrl}/error-codes/get_error_codes`, params);
  }
}
