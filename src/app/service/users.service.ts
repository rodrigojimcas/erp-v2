import { environment } from '../../environments/environment';
import { HttpClient,  HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs/';
@Injectable({
  providedIn: 'root'
})
export class UsersService {

  baseUrl = `${environment.apiUrl}`;

  constructor(
    private http: HttpClient,
  ) {
  }

  user(val) {
    // const response = new ReplaySubject<any>(1);
    return this.http.get(`${this.baseUrl}/users/` + val);
  }

  getAdmins(payload) {
    return this.http.post(`${this.baseUrl}/users/administrators`, payload);
  }

  create(credentials) {
    // const response = new ReplaySubject<any>(1);
    return this.http.post(`${this.baseUrl}/users/`, credentials);
  }

  // destroy(id) {
  //   const response = new ReplaySubject<any>(1);
  //   this.http.destroy('administrator/users/' + id);
  // }

  show(id) {
    // const response = new ReplaySubject<any>(1);
    return this.http.get(`${this.baseUrl}/users/${id}`);
  }

  /*   update(id, data) {
      const response = new ReplaySubject<any>(1);
      this.apiService.put('administrator/users/' + id, data).subscribe(dataResponse => {
        response.next(dataResponse);
      });
      return response;
    } */
}
