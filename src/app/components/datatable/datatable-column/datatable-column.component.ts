import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-datatable-column',
  templateUrl: './datatable-column.component.html',
  styleUrls: ['./datatable-column.component.scss']
})

export class DatatableColumnComponent implements OnInit {
  @Input() column:any;
  @Input() data: any;

  constructor() { }

  ngOnInit() {}

  getField(field: string, data) {
    const nField = field.split('.');
    let nData = data;
    nField.forEach((item:any) => {
      nData = data[item];
    });
    return nData;
  }
  
}
