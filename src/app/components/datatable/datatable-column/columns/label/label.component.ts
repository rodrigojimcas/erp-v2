import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-datatable-column-label',
  templateUrl: './label.component.html',
  styleUrls: ['./label.component.scss']
})
export class LabelComponent implements OnInit {
  @Input() data: any;
  
  constructor() { }

  ngOnInit() {
  }

}
