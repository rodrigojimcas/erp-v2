import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-datatable-column-ticket-status',
  templateUrl: './ticket-status.component.html',
  styleUrls: ['./ticket-status.component.scss']
})
export class TicketStatusComponent implements OnInit {
  @Input() data: any;

  constructor() { }

  ngOnInit() {
  }

}
