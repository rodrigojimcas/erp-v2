import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { BroadcastService } from '../../../../../service/broadcast.service';
import { DataTableDirective } from 'angular-datatables';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit {
    @Input() data: any;
    @ViewChild(DataTableDirective, {static: false}) datatableElement: DataTableDirective;

    selected;

    constructor(private broadcast: BroadcastService) {}

    ngOnInit() { }

    isVisible(active) {
        return eval(active);
    }

    radioChange() {
        this.broadcast.fire({
            name: 'datatable-filter', 
            data: { 
                value: this.selected, 
                type: this.data.type
            }
        });
    } 
}