import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-datatable-column-ticket-client',
  templateUrl: './ticket-client.component.html',
  styleUrls: ['./ticket-client.component.scss']
})
export class TicketClientComponent implements OnInit {
  @Input() data: any;

  constructor() { }

  ngOnInit() {
  }

}
