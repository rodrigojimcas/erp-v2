import { Component, OnInit, Input } from '@angular/core';
import { BroadcastService } from '../../../../../service/broadcast.service';


@Component({
  selector: 'app-datatable-column-actions',
  templateUrl: './actions.component.html',
  styleUrls: ['./actions.component.scss']
})
export class ActionsComponent implements OnInit {
  @Input() data: any;
  @Input() options: any;

  constructor(private broadcast: BroadcastService) {}

  ngOnInit() {}

  isVisible(conditionallity) {
    return eval(conditionallity);
  }

  fireEvent(event) {
    this.broadcast.fire({name: event, data: this.data});
  }

  // onCreateModal(): void {
  //   const modalRef = this.datatable.open(Error, { title: 'My dynamic title', message: 'My dynamic message' });

  //   modalRef.onResult().subscribe(
  //     closed => console.log('closed', closed),
  //     dismissed => console.log('dismissed', dismissed),
  //     () => console.log('completed')
  //   );
  // }

}
