import { Component, OnDestroy, OnInit, ViewChild, Input, AfterViewInit } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { TicketsService } from '../../service/tickets.service';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { DataTableDirective } from 'angular-datatables';
import { BroadcastService } from '../../service/broadcast.service';

@Component({
  selector: 'app-datatable',
  templateUrl: './datatable.component.html',
  styleUrls: ['./datatable.component.scss']
})

export class DatatableComponent implements OnInit, OnDestroy, AfterViewInit {
  
  @Input('options') options: any;

  @ViewChild(DataTableDirective, {static: false}) datatableElement: DataTableDirective;
  
  dtService: Subscription;
  dtTrigger: Subject<any> = new Subject();
  dtOptions: DataTables.Settings = {};
  filters = {};
  items = [];
  ticketAction: number;

  constructor(private broadcast: BroadcastService, private tickets: TicketsService ) {}

  ngOnInit(): void {
    
    const dtOpts = {
      pagingType: 'full_numbers',
      pageLength: 10,
      serverSide: true,
      processing: true,
      language: {
        url: '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json'
      },
      ajax: (dataTablesParameters: any, callback) => {
        const baseService = this.options.config.base;
        const baseFunction = this.options.config.api;
        // const baseId = this.options.id;
        // let base;
        // if (baseId) {
        //   base = [`${baseFunction}`][baseId];
        // } else {
        //   base = [`${baseFunction}`];
        // }
        // console.log('base',base);
        if (dataTablesParameters) {
          
          baseService[`${baseFunction}`](dataTablesParameters, this.filters)
          // baseService[`${base}`](dataTablesParameters, this.filters)
            .subscribe((resp:any) => {
              this.items = resp.data;
              callback({
                recordsTotal: resp.recordsTotal,
                recordsFiltered: resp.recordsFiltered,
                data: []
              });
            });
        }            
      }
    };

    if ('order' in this.options.config) {
      Object.assign(dtOpts, {order: this.options.config.order});
    }
    
    if ('params' in this.options.config) {
      Object.assign(this.filters, this.options.config.params);
    } 
    this.dtOptions = dtOpts;

    this.dtService = this.broadcast.events.subscribe(event => {
      switch(event.name) {
        case 'datatable-filter': this.filterChange(event.data); break;
      }
    });
  }

  ngAfterViewInit(): void {
    // this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  onClickAction(value) {
    this.ticketAction = value;
  }

  filterChange(params) {
    Object.assign(this.filters, {[params.type]: params.value});
    this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.draw();
    });
  }
  
}
