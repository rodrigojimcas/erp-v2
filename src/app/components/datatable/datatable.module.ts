import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DataTablesModule } from 'angular-datatables'
import { DatatableComponent } from './datatable.component';
import { ModalModule } from '../modal/modal.module';
import { DatatableColumnComponent } from './datatable-column/datatable-column.component';
import { TicketStatusComponent } from './datatable-column/columns/ticket-status/ticket-status.component';
import { TicketClientComponent } from './datatable-column/columns/ticket-client/ticket-client.component';
import { TicketContactComponent } from './datatable-column/columns/ticket-contact/ticket-contact.component';
import { ActionsComponent } from './datatable-column/columns/actions/actions.component';
import { LabelComponent } from './datatable-column/columns/label/label.component';
import { FiltersComponent } from './datatable-column/columns/filters/filters.component';
// import { NgbdSortableHeader, NgbdTableSortable } from './table-sortable';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        DataTablesModule,
        // DatatableModule,
        // ModalModule
    ],
    declarations: [
        DatatableComponent,
        DatatableColumnComponent,
        TicketStatusComponent,
        TicketClientComponent,
        TicketContactComponent,
        ActionsComponent,
        LabelComponent,
        FiltersComponent
    ],
    entryComponents: [],
    exports: [
        DatatableComponent
    ],
    providers: [],
    bootstrap: []
})

export class DatatableModule { }
