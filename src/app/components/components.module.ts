import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DatatableModule } from './datatable/datatable.module';
import { ModalModule } from './modal/modal.module';
import { FormModule } from './form/form.module';

@NgModule({
  declarations: [
  ],
  entryComponents: [
  ],
  exports: [
    DatatableModule,
    ModalModule,
    FormModule
  ],
  imports: [
    CommonModule,
    DatatableModule,
    ModalModule,
    FormModule
  ],
})
export class ComponentsModule { }