import { Component, OnInit, Input, ViewChild, ComponentFactoryResolver } from '@angular/core';
import { SharedComponentDirective } from '../../../directives/shared-component.directive';
import { ISharedComponent } from '../../../model/shared-component.interface';
import { SharedComponent } from '../../../model/shared-component';

@Component({
  selector: 'app-modal-body',
  templateUrl: './modal-body.component.html',
  styleUrls: ['./modal-body.component.scss']
})
export class ModalBodyComponent implements OnInit {
  @Input() props: SharedComponent;
  @ViewChild(SharedComponentDirective, {static: true}) sharedComponent: SharedComponentDirective;

  constructor(private componentFactoryResolver: ComponentFactoryResolver) { }

  ngOnInit() {
    const cmpFctory = this.componentFactoryResolver.resolveComponentFactory(this.props.component);
    const viewContainer = this.sharedComponent.viewContainerRef;
    viewContainer.clear();

    const cmpRef = viewContainer.createComponent(cmpFctory);
    (<ISharedComponent>cmpRef.instance).data = this.props.data;
  }

}
