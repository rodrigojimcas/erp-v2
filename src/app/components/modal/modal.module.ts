import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalComponent } from './modal.component';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DataTablesModule } from 'angular-datatables';
import { ModalBodyComponent } from './modal-body/modal-body.component'
import { EditComponent } from '../../admin/error-codes/edit/edit.component';
import { RecordComponent } from '../../admin/tickets/record/record.component';
import { AssignComponent } from '../../admin/tickets/assign/assign.component';
import { ReassingComponent } from '../../admin/tickets/reassing/reassing.component';
import { SharedComponentDirective } from '../../directives/shared-component.directive';
import { ContractComponent } from '../../admin/clients/contract/contract.component';
import { OfflinePaymentComponent } from '../../admin/clients/contract/offline-payment/offline-payment.component';
import { ClientEditComponent } from '../../admin/clients/client-edit/client-edit.component';
import { DatatableModule } from '../datatable/datatable.module';
import { FromService } from '../../providers/form.service';
import { FormModule } from '../form/form.module';
import { ReferalsComponent } from '../../admin/clients/referals/referals.component';
import { TreeviewComponent } from '../../admin/clients/referals/treeview/treeview.component';
import { AdministratorCreateEditComponent } from '../../admin/administrators/administrator-create-edit/administrator-create-edit.component';


const modalBodys = [
  EditComponent,
  RecordComponent,
  AssignComponent,
  ReassingComponent,
  ContractComponent,
  OfflinePaymentComponent,
  ClientEditComponent,
  AdministratorCreateEditComponent,
  ReferalsComponent
]

@NgModule({
  declarations: [
    ModalComponent,
    ModalBodyComponent,
    SharedComponentDirective,
    TreeviewComponent,
    ...modalBodys
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    // DataTablesModule,
    DatatableModule,
    FormModule
  ],
  entryComponents: [
    ModalComponent,
    ModalBodyComponent,
    TreeviewComponent,
    ...modalBodys
  ],
  exports: [
    ModalComponent
  ],
  providers:[
    ModalComponent,
    NgbActiveModal,
    FromService
  ],
  bootstrap: []
})
export class ModalModule { }