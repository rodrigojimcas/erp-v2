import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SharedComponent } from '../../model/shared-component';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
  @Input() props: any;
  @Input() config: any;

  constructor(public activeModal: NgbActiveModal, private modalService: NgbModal) { }

  ngOnInit() {}

  open(props: SharedComponent) {
    const modalRef = this.modalService.open(ModalComponent/*, { windowClass: 'dark-modal' }*/);
    modalRef.componentInstance.props = props;
    // modalRef.componentInstance.props = props.data;
    modalRef.componentInstance.config = props.config;
  }

  openXl(props: SharedComponent) {
     const modalRef = this.modalService.open(ModalComponent, {size: 'xl'});
     modalRef.componentInstance.props = props;
     modalRef.componentInstance.config = props.config;
  }
}
