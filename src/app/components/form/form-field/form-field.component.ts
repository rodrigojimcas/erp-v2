import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { FromService } from '../../../providers/form.service';
import { DomSanitizer, SafeScript } from '@angular/platform-browser';

@Component({
  selector: 'app-form-field',
  templateUrl: './form-field.component.html',
  styleUrls: ['./form-field.component.scss']
})
export class FormFieldComponent implements OnInit {
  @Input() type: string;
  @Input() options: any;

  public field: FormControl;
  private image: any;
  
  constructor(private fromService: FromService, private sanitizer: DomSanitizer) { }
  
  ngOnInit() {
    // console.log('este campo', this.options.path, this.field);
    this.field = this.form.get(this.options.path) as FormControl;
  }
  get form(): FormGroup { 
    return this.fromService.form; 
  }

  get isChecked(): boolean {
    return this.field.value == (this.options.hasOwnProperty('trueValue') ? this.options.trueValue : true)
  }
  
  bgImage(): String {
    return this.image ||'http://cliquecities.com/assets/no-image-e3699ae23f866f6cbdf8ba2443ee5c4e.jpg'
  }

  bgStyle(): any {
    const imageString =  JSON.stringify(this.bgImage()).replace(/\\n/g, '');
    const style = 'url(' + imageString + ')';
    return this.sanitizer.bypassSecurityTrustStyle(style);
    // return 'url(' + this.bgImage + ');';
  }

  switched(e): void {
    const target = e.target;
    this.field.setValue(target.checked ? 
      (this.options.hasOwnProperty('trueValue') ? this.options.trueValue : true) : 
      (this.options.hasOwnProperty('falseValue') ? this.options.falseValue : false))
  }


  readFile(e): void {
    let input = e.target;
    if (input.files && input.files[0]) {
      let reader: FileReader = new FileReader();
      reader.onload = (ev: any) => {
          this.image = ev.target.result
          this.field.setValue(ev.target.result)
      }
      reader.readAsDataURL(input.files[0]);
    }
  }

}
