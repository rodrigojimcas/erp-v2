import { AuthGuardService } from './service/auth-guard.service';
import { DefaultLayoutComponent } from './containers/default-layout/default-layout.component';
import { Routes } from '@angular/router';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: 'login',
    loadChildren: './session/session.module#SessionModule'
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    children: [{
      path: 'admin',
      loadChildren: './admin/admin.module#AdminModule',
      canActivate: [AuthGuardService]
    }],
  },
  // {
  //   path: '**',
  //   redirectTo: 'session'
  // },
];