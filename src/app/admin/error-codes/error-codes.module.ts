import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ComponentsModule } from '../../components/components.module';
import { ErrorCodesComponent } from './error-codes.component';


@NgModule({
  declarations: [
    ErrorCodesComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    ComponentsModule
  ],
  exports: [
    ErrorCodesComponent
  ]
})
export class ErrorCodesModule { }
