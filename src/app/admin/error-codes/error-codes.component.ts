import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { TicketsService } from '../../service/tickets.service';
import { BroadcastService } from '../../service/broadcast.service';
import Swal from 'sweetalert2'
import { ModalComponent } from '../../components/modal/modal.component';
import { SharedComponent } from '../../model/shared-component';
import { EditComponent } from './edit/edit.component';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-error-codes',
  templateUrl: './error-codes.component.html',
  styleUrls: ['./error-codes.component.scss']
})

export class ErrorCodesComponent implements OnDestroy, OnInit {
  public dataTableConfig = {
    config: {
      base: this.tickets,
      api: 'getErrorCodes'
    },
    columns:[
      {
        display: 'Nombre',
        field: 'name',
        type: 'text'
      },
      {
        display: 'Categoría',
        field: 'category',
        type: 'text'
      },
      {
        display: 'Codigo',
        field: 'code',
        type: 'text'
      },
      {
        display: 'Fecha de alta',
        field: 'created_at',
        type: 'date'
      },
      {
        display: 'Acciones',
        field: '',
        type: 'actions',
        options: [
          {
            display: 'Editar',
            icon:'fa fa-pencil',
            event: 'error_codes.edit',
            conditionality: 'true'
          },
          {
            display: 'Eliminar',
            icon:'fa fa-trash fa-fw',
            event: 'error_codes.delete',
            conditionality: 'true'
          }
        ]

      }
    ]
  };
  
  broadcast$: Subscription;

  constructor(
    private tickets: TicketsService, 
    private broadcast: BroadcastService, 
    public appModal: ModalComponent){}

  ngOnInit(): void {
    this.broadcast$ = this.broadcast.events.subscribe(event => {
      console.log("clic en boton de error code");
      console.log(event);
      switch(event.name) {
        case 'error_codes.delete': this.deleteItem(event.data); break;
        case 'error_codes.edit': this.editItem(event.data); break;
      }
    });
  }

  ngOnDestroy(): void {
    this.broadcast$.unsubscribe();
  }
  
  deleteItem(data) {
    Swal.fire({
      title: '¿Estás seguro de eliminar el código de error?',
      icon: 'warning',
      showCancelButton: true,
      text: '',
      confirmButtonColor: '#128f76',
      confirmButtonText: 'Si, eliminar.',
      cancelButtonText: 'Cancelar'
    })
  }
  
  editItem(data) {
    // open modal, passing the context
    const props: SharedComponent = new SharedComponent(EditComponent, data, {title:'Códigos de error'});
    this.appModal.open(props);
  }

}
