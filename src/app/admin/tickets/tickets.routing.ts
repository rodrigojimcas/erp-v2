import {Routes} from '@angular/router';
import {AuthGuardService} from '../../service/auth-guard.service';
import { TicketsComponent } from './tickets.component';

export const TicketsRoutes: Routes = [
    {
        path: '',
        component: TicketsComponent
    },
    {
        path: '',
        children: [
            {path: 'tickets', component: TicketsComponent}
        ]
    }
];