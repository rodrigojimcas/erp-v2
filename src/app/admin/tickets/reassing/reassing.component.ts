import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-reassing',
  templateUrl: './reassing.component.html',
  styleUrls: ['./reassing.component.scss']
})
export class ReassingComponent implements OnInit {
  @Input() data: any;

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
    console.log(this.data);
  }

}
