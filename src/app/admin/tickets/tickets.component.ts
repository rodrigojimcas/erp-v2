import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { TicketsService } from '../../service/tickets.service';
import { BroadcastService } from '../../service/broadcast.service';
import { ModalComponent } from '../../components/modal/modal.component';
import { SharedComponent } from '../../model/shared-component';
import { RecordComponent } from './record/record.component';
import { AssignComponent } from './assign/assign.component';
import { ReassingComponent } from './reassing/reassing.component';
import { Subscription } from 'rxjs';
import Swal from 'sweetalert2'


@Component({
  selector: 'app-tickets',
  templateUrl: './tickets.component.html',
  styleUrls: ['./tickets.component.scss']
})

export class TicketsComponent implements OnDestroy, OnInit {
  public dataTableConfig = {
    config: {
      base: this.tickets,
      api: 'getTickets',
      order: [[ 0, "desc" ]]
    },
    columns: [
      {
        display: 'Estatus',
        field: 'id',
        type: 'ticket_status'
      },
      {
        display: 'CLIENTE',
        field: 'client.name',
        type: 'ticket_client'
      },
      {
        display: 'CONTACTO',
        field: 'client',
        type: 'ticket_contact'
      },
      {
        display: 'FECHA DE CREACION',
        field: 'created_at',
        type: 'date'
      },
      {
        display: 'ACCIONES',
        field: '',
        type: 'actions',
        options: [
          {
            display: 'Historial',
            icon:'fa fa-search',
            event: 'tickets.record',
            conditionality: 'true'
          },
          {
            display: 'Asignar ticket',
            icon:'fa fa-hand-o-right',
            event: 'tickets.assign',
            conditionality: 'this.data.status === "opened"'
          },
          {
            display: 'Reasignar ticket',
            icon:'fa fa-hand-o-right',
            event: 'tickets.reassing',
            conditionality: 'this.data.status === "assigned"'
          },
          {
            display: 'Desasignar ticket',
            icon: 'fa fa-hand-o-left',
            event: 'tickets.unassign',
            conditionality: 'this.data.status === "assigned"'
          },
          {
            display: 'Cerrar ticket',
            event: 'tickets.close',
            conditionality: 'this.data.status === "completed" || this.data.status === "confirmed" '
          }
        ]
      }
    ],
    filters: [
      {
        cssClass: 'col-md-12',
        type: 'status',
        options: [
          {
            val: "",
            name: "TODOS"
          },
          {
            val: "opened",
            name: "ABIERTO"
          },
          {
            val: "assigned",
            name: "ASIGNADO"
          },
          {
            val: "completed",
            name: "COMPLETADO POR EL TECNICO"
          },
          {
            val:"confirmed",
            name: "FIRMADO POR EL CLIENTE"
          },
          {
            val:"closed",
            name: "CERRADO"
          }
        ]
      }
    ]
  };

  broadcast$: Subscription;

  constructor(
    private tickets: TicketsService,
    private broadcast: BroadcastService,
    public appModal: ModalComponent) {}

  ngOnInit(): void {

    this.broadcast$ = this.broadcast.events.subscribe(event => {
      switch(event.name) {
        case 'tickets.record': this.recordItem(event.data); break;
        case 'tickets.assign': this.assignItem(event.data); break;
        case 'tickets.reassing': this.reassingItem(event.data); break;
        case 'tickets.unassign': this.unassignItem(event.data); break;
        case 'tickets.close': this.closeItem(event.data); break;
      }
    });

  }

  ngOnDestroy(): void {
    this.broadcast$.unsubscribe();
  }

  recordItem(data){
    // open modal, passing the context
    const props: SharedComponent = new SharedComponent(RecordComponent, data, {title: `Historial del ticket ${data.id_tickets} del cliente ${data.client.name}`});
    this.appModal.open(props);
  }
  
  assignItem(data){
    // open modal, passing the context
    const props: SharedComponent = new SharedComponent(AssignComponent, data, {title: 'Asignación de ticket'});
    this.appModal.open(props);
  }

  reassingItem(data){
    // open modal, passing the context
    const props: SharedComponent = new SharedComponent(ReassingComponent, data, {title: 'Asignación de ticket'});
    this.appModal.open(props);
  }

  unassignItem(data){
    Swal.fire({
      title: '¿Desea desasignar este ticket?',
      showCancelButton: true,
      text: '¡Si desasigna este ticket el estatus pasara a abierto!',
      confirmButtonColor: '#128f76',
      confirmButtonText: 'Confirmar',
      cancelButtonText: 'Cancelar'
    })
  }

  closeItem(data){
    // open modal, passing the context
    const props: SharedComponent = new SharedComponent(RecordComponent, data, {title: `Cerrar el ticket ${data.id_tickets} del cliente ${data.client.name}`});
    this.appModal.open(props);
  }
  
}
