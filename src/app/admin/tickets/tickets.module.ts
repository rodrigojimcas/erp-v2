import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { TicketsComponent } from './tickets.component';
import { TicketsRoutes } from './tickets.routing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ComponentsModule } from '../../components/components.module';
//import { NgbdSortableHeader, NgbdTableSortable } from './table-sortable';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(TicketsRoutes),
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        ComponentsModule
    ],
    declarations: [
        TicketsComponent
        //NgbdSortableHeader,
        //NgbdTableSortable
    ],
    entryComponents: [],
    exports: [
        TicketsComponent,
      //  NgbdTableSortable
    ],
    providers:[],
    bootstrap: [
       // NgbdTableSortable
    ]
})

export class TicketsModule {}