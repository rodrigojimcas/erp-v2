import { Component, OnInit, OnDestroy } from '@angular/core';
import { BroadcastService } from '../../service/broadcast.service';
import { ModalComponent } from '../../components/modal/modal.component';
import { ClientsService } from '../../service/clients.service';
import { Subscription } from 'rxjs';
import { ClientEditComponent } from './client-edit/client-edit.component';
import { ContractComponent } from '../../admin/clients/contract/contract.component'
import { SharedComponent } from '../../model/shared-component';
import { BadDebtsService } from '../../service/bad-debts.service';
import { SwalService } from '../../service/swal.service';
import { ReferalsComponent } from './referals/referals.component';
@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss']
})
export class ClientsComponent implements OnDestroy, OnInit {
  public dataTableConfig = {
    config: {
      base: this.clients,
      api: 'getClients'
    },
    columns: [
      {
        display: 'Nombre',
        field: 'name',
        type: 'text'
      },
      {
        display: 'Grupo de ventas',
        field: 'signed_in_group.name',
        type: 'text'
      },
      {
        display: 'Fecha de alta',
        field: 'created_at',
        type: 'date'
      },
      {
        display: 'Suscripción',
        field: 'subscription_status',
        type: 'text'
      },
      {
        display: 'Email',
        field: 'email',
        type: 'text'
      },
      {
        display: 'Estatus',
        field: 'status',
        type: 'text'
      },
      {
        display: 'Sig. Pago',
        field: 'next_payday',
        type: 'date'
      },
      {
        display: 'ACCIONES',
        field: '',
        type: 'actions',
        options: [
          {
            display: 'Ver contrato de compra',
            icon: 'fa fa-file-text',
            event: 'client.contract',
            conditionality: 'true'
          },
          {
            display: 'Ver contratos referidos',
            icon: 'fa fa-file-o',
            event: 'client.referals',
            conditionality: 'this.data.status ===  "accepted"'
          },
          {
            display: 'Historial del cliente',
            icon: 'fa fa-search',
            event: 'tickets.reassing',
            conditionality: 'true'
          },
          {
            display: 'Crear cargo OXXO/SPEI',
            icon: 'fa fa-dollar',
            event: 'tickets.unassign',
            conditionality: 'this.data.status !== "canceled" || this.data.status !== "rejected"'
          },
          {
            display: 'Ver cargos',
            icon: 'fa fa-dollar',
            event: 'tickets.unassign',
            conditionality: 'this.data.status !== "rejected"'
          },
          {
            display: 'Editar',
            icon: 'fa fa-pencil',
            event: 'client.clientEdit',
            conditionality: 'true'
          },
          {
            display: 'Restablecer contraseña',
            icon: 'fa fa-key',
            event: 'tickets.close',
            conditionality: 'true'
          },
          {
            display: 'Regenerar usuario en CONEKTA',
            icon: 'fa fa-repeat',
            event: 'tickets.close',
            conditionality: 'this.data.status !== "canceled" || this.data.status !== "rejected"'
          },
          {
            display: 'Activar como agente de Ventas',
            icon: 'fa fa-handshake-o',
            event: 'client.makeSalesAgent',
            conditionality: 'this.data.sales_agent !== 1'
          },
          {
            display: 'Desactivar como agente de Ventas',
            icon: 'fa fa-handshake-o',
            event: 'client.removeSalesAgent',
            conditionality: 'this.data.sales_agent === 1'
          },
          {
            display: 'Aceptar cliente',
            event: 'client.setAccepted',
            conditionality: 'this.data.status === "standby"'
          },
          {
            display: 'Invalidar cliente',
            event: 'client.setInvalid',
            conditionality: 'this.data.status === "standby"'
          },
          {
            display: 'Rechazar cliente',
            event: 'client.setRejected',
            conditionality: 'this.data.status === "standby"'
          },
          {
            display: 'Cancelar cliente',
            event: 'client.setCancelled',
            conditionality: 'this.data.status === "rejected"'
          },
          {
            display: 'Mover a mala deuda',
            event: 'client.moveToBadDebt',
            conditionality: 'this.data.bad_debt === 0'
          },
          {
            display: 'Crear ticket de soporte',
            icon: 'fa fa-ticket',
            event: 'tickets.close',
            conditionality: 'true'
          }
        ]
      }
    ],
    filters: [
      {
      cssClass: 'col-md-4',
      type: 'status',
      options: [
        {
          val: "",
          name: "TODOS"
        },
        {
          val: "standby",
          name: "Pendiente"
        },
        {
          val: "invalid",
          name: "Invalido"
        },
        {
          val: "canceled",
          name: "Cancelado"
        },
        {
          val: "rejected",
          name: "Rechazado"
        },
        {
          val: "accepted",
          name: "Aceptado"
        }
      ]
    },
    {
      cssClass: 'col-md-4',
      type: 'serial_number',
      options: [
        {
          val: "",
          name: "TODOS"
        },
        {
          val: "instaled",
          name: "Instalado"
        },
        {
          val: "",
          name: "No instalado"
        }
      ]
    },
    {
      cssClass: 'col-md-4',
      type: 'sales_agent',
      options: [
        {
          val: "",
          name: "TODOS"
        },
        {
          val: "1",
          name: "Activo"
        },
        {
          val: "0",
          name: "Inactivo"
        }
      ]
    }
  ]
  };
  
  broadcast$: Subscription;

  constructor(
    private clients: ClientsService,
    private badDebts: BadDebtsService,
    private broadcast: BroadcastService,
    public appModal: ModalComponent,
    public swal: SwalService,
  ) { }

  ngOnInit(): void {
    this.broadcast$ = this.broadcast.events.subscribe(event => {
      switch (event.name) {
        case 'client.contract': this.contractItem(event.data); break;
        case 'client.referals': this.referalsItem(event.data); break;
        case 'client.clientEdit': this.clientEditItem(event.data); break;
        case 'client.makeSalesAgent': this.makeSalesAgentItem(event.data); break;
        case 'client.removeSalesAgent': this.removeSalesAgentItem(event.data); break;
        case 'client.setAccepted': this.setAcceptedItem(event.data); break;
        case 'client.setInvalid': this.setBadStatusItem(event.data, event.name); break;
        case 'client.setRejected': this.setBadStatusItem(event.data, event.name); break;
        case 'client.setCancelled': this.setBadStatusItem(event.data, event.name); break;
        case 'client.moveToBadDebt': this.moveToBadDebt(event.data); break;
      }
    });
  }

  ngOnDestroy() {
    this.broadcast$.unsubscribe();
  }

  contractItem(data) {
    const props: SharedComponent = new SharedComponent(ContractComponent, data, { title: `Contrato de compra del cliente ${data.name}` });
    this.appModal.openXl(props);
  }

  referalsItem(data) {
    const props: SharedComponent = new SharedComponent(ReferalsComponent, data, { title: `Contratos referidos del cliente ${data.name}` });
    this.appModal.openXl(props);
  }

  clientEditItem(data) {
    const props: SharedComponent = new SharedComponent(ClientEditComponent, data, { title: `Edición del cliente` });
    this.appModal.openXl(props);
  }

  offlinePaymentItem(data) {

  }

  setAcceptedItem(data) {
    let params = { 'status': 'accepted' }

    this.swal.actionConfirmation('¿Estás seguro de aceptar al cliente?').then((result) => {
      if (result.value) {
        this.clients.setStatus(data.id_clients, params).subscribe((data: any) => { });
        this.swal.success();
      }
    });
  }

  async setBadStatusItem(data, event) {

    let title_status: string;
    let status: string;

    switch (event) {
      case 'client.setCancelled':
        title_status = 'cancelado';
        status = 'canceled';
        break;

      case 'client.setInvalid':
        title_status = 'invalidado';
        status = 'invalid';
        break;

      case 'client.setRejected':
        title_status = 'rechazado';
        status = 'rejected';
        break;
    }

    const { value: reason } = await this.swal.inputSwal('Motivos', 'Favor de proporcionar las razones por las que el cliente será ' + title_status);
    if (reason) {
      let params = { 'status': status, 'reasons': reason }
      this.clients.setStatus(data.id_clients, params).subscribe((response: any) => {
        if (response.success) {
          this.swal.success();
        } else {
          this.swal.error(response.message);
        }
      });
      this.swal.success();
    } else {
      this.swal.error('Debe de especificar las razones');
    }
  }

  async moveToBadDebt(data) {
    const { value: reason } = await this.swal.inputSwal('Mover a mala deuda', 'Favor de proporcionar las razones por las que el cliente sera movido a mala deuda');
    if (reason) {
      let params = { 'reason': reason };
      this.badDebts.moveToBadDebt(data.id_clients, params).subscribe((response: any) => {
        if (response.success) {
          this.swal.success();
        } else {
          this.swal.error(response.message);
        }
      });
    } else {
      this.swal.error('Debe de especificar las razones');
    }
  }

  makeSalesAgentItem(data) {
    let params = { 'role': 'Sales Agent' }
    let title = 'Estás seguro de cambiar el nivel del cliente ' + data.name;

    this.swal.actionConfirmation(title).then((result) => {
      if (result.value) {
        this.clients.makeSalesAgent(data.id_clients, params).subscribe((response: any) => {
          if (response.success) {
            this.swal.success();
          } else {
            this.swal.error(response.message);
          }
        });
        this.swal.success();
      }
    });
  }

  removeSalesAgentItem(data) {

  }
}
