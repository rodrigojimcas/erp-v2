import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { environment } from '../../../../../environments/environment'
import { ClientsService } from '../../../../service/clients.service';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-offline-payment',
  templateUrl: './offline-payment.component.html',
  styleUrls: ['./offline-payment.component.scss']
})
export class OfflinePaymentComponent implements OnInit {
  @Input() data: any;
  offline_form: FormGroup;
  option_value;
  months_ahead = environment.months_ahead;
  payment_types = environment.payment_types;

  offlinePaymentData = {
    id_clients: 0,
  }

  broadcast$: Subscription;

  constructor(
    private formBuilder: FormBuilder,
    private offlinePaymentService: ClientsService
  ) { }

  ngOnInit() {
    this.offlinePaymentData.id_clients = this.data.client.id_clients;
    this.offlineForm();
  }

  ngOnDestroy(): void {
    this.broadcast$.unsubscribe();
  }

  offlineForm() {
    this.offline_form = this.formBuilder.group({
      months_ahead: ['', Validators.required],
      payment_type: ['', Validators.required],
      id_clients: [this.offlinePaymentData.id_clients, Validators.required]
    });
  }

  offlinePayment() {

    if (this.offline_form.valid) {

      if (this.data.firstCharge) {
        this.offlinePaymentService.subscribeToPlanOffline(this.offlinePaymentData.id_clients, this.offline_form.value).subscribe((data: any) => {
          // console.log(data);
        })
      }
      // console.log('Pago offline', params)
    }

  }

  totalOffline() {

    let contract_total = this.data.contract.total;
    let sr = this.data.contract.sr;
    let monthly_fee = this.data.contract.monthly_fee;
    let months = this.offline_form.get('months_ahead').value;
    // console.log(contract_total);
    let total = Math.round((((monthly_fee + sr) * months) + (contract_total + 100)) * 100) / 100;

    return total;
  }

  onChage(value) {
    console.log('valor', value);
  }

}
