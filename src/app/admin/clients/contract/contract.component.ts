import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ClientsService } from '../../../service/clients.service';
import { SharedComponent } from '../../../model/shared-component';
import { OfflinePaymentComponent } from './offline-payment/offline-payment.component';
import { ModalComponent } from '../../../components/modal/modal.component';
import { Subscription } from 'rxjs';
import { BroadcastService } from '../../../service/broadcast.service';
import { SwalService } from '../../../service/swal.service';
@Component({
  selector: 'app-contract',
  templateUrl: './contract.component.html',
  styleUrls: ['./contract.component.scss']
})
export class ContractComponent implements OnInit, OnDestroy {
  @Input() data: any;
  refeer: any;
  coupon = {};
  images = [];
  client: any;
  contractData = {
    id_clients: 0,
    deposit: 0,
    monthly_fee: 0,
    first_charge: 0,
    extras: 0,
    sr: 0,
    bank_fee: 0,
    installation_fee: 0,
    coupon: 0,
    total: 0
  };

  public dataTableConfig = {
    config: {
      base: this.clientService,
      api: 'getCards',
      params: {
        id_clients: 0
      }
    },
    columns: [
      {
        display: 'Nombre del tarjetahabiente',
        field: 'name',
        type: 'text'
      },
      {
        display: 'Fecha de alta',
        field: 'created_at',
        type: 'date'
      },
      {
        display: 'Expiración',
        field: 'exp_date',
        type: 'text'
      },
      {
        display: 'Últimos 4 digitos',
        field: 'last4',
        type: 'text'
      },
      {
        display: 'Marca',
        field: 'brand',
        type: 'text'
      },
    ],
    filters: [{}]
  };

  broadcast$: Subscription;
  
  constructor(
    private clientService: ClientsService,
    private swal: SwalService,
    private broadcast: BroadcastService,
    public activeModal: NgbActiveModal,
    public appModal: ModalComponent,
  ) {
  }

  ngOnInit() {
    this.clientService.show(this.data.id_clients).subscribe((resp: any) => {
      this.client = resp.response;
      console.log(this.client.cards.length);
      this.contractData.id_clients = this.client.id_clients;
      this.broadcast$ = this.broadcast.events.subscribe(eventData => {});
      this.contractData.deposit = this.client.deposit / 100;
      this.contractData.monthly_fee = this.client.monthly_fee / 100;
      this.contractData.installation_fee = this.client.installation_fee / 100;
      this.contractData.first_charge = this.firstCharge();
      this.contractData.extras = this.extras();
      this.contractData.sr = Math.round(this.socialResponsability() * 100) / 100;
      this.contractData.bank_fee = this.client.charge_fee / 100;
      this.contractData.coupon = this.discountCoupon();
      this.contractData.total = this.total();
      this.dataTableConfig.config.params.id_clients = this.client.id_clients;
      this.getRefeer();
    });
  }

  ngOnDestroy() {
    this.broadcast$.unsubscribe();
  }

  extras() {
    let totalPrice = 0;
    for (let i = 0; i < this.client.contract.extras.length; i++) {
      let extra = this.client.contract.extras[i];
      let materialPrice = Number(extra.price);
    
      if (extra.element_name === 'Acero inoxidable') {
        materialPrice = this.contractData.installation_fee;
      }

      totalPrice += materialPrice;
    }
    return totalPrice;
  }

  firstCharge() {
    let first_charge = this.contractData.monthly_fee;

    if (this.client.signed_in_group !== null && 'trial_days_price' in this.client.signed_in_group) {
      let trial_days_price = parseInt(this.client.signed_in_group.trial_days_price) / 100;

      // if (this.client.group.trial_days !== 0) {
      if (this.client.signed_in_group.trial_days !== 0) {
        first_charge = trial_days_price;
      }
    }
    return first_charge;
  }

  socialResponsability(): number {
    let social_responsability = 0, total = 0;

    for (let i = 0; i < this.client.contract.extras.length; i++) {
      let extra = this.client.contract.extras[i];
      let materialPrice = Number(extra.price);

      if (extra.element_name === 'Acero inoxidable') {
        materialPrice = this.contractData.installation_fee;
      }

      // social_responsability = ((materialPrice + this.contractData.monthly_fee) / 1.16 * .007);
      social_responsability = ((materialPrice) / 1.16 * .007);
      total += social_responsability;
    }
    return social_responsability;
  }

  discountCoupon() {
    let discount = 0;
    if (this.client.id_coupon > 0) {
      discount = parseInt(this.client.coupon.reward);
    }
    return discount;
  }

  getRefeer() {
    this.clientService.show(this.client.referred_by).subscribe((data: any) => {
      this.refeer = data.response;
    });
  }

  total() {

    let total = (this.contractData.deposit + this.contractData.first_charge + this.contractData.sr + this.contractData.extras) - this.contractData.coupon;

    return total;
  }
  
  firstPayment() {
    this.swal.actionConfirmation('¿Estás seguro de continuar? se intentará recolectar el primer pago del cliente').then((result) => {
        if (result.value) {
          this.clientService.subscribeToPlan(this.client.id_clients).subscribe((data: any) => {
            if (data.success) {
              this.swal.success().then(() => {
                this.activeModal.dismiss();
              });
            } else {
              this.swal.error('Ocurrior un error al procesar el pago');
            }
          });
        }
    });
  }
  
  offlinePayment() {
    const props: SharedComponent = new SharedComponent(
      OfflinePaymentComponent,
      {
        client: this.data,
        contract: this.contractData,
        firstCharge: true
      },
      {
        title: `Pago por OXXO / SPEI`
      }
    );
    this.appModal.openXl(props);
  }
}
