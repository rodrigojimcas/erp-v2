import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { FromService } from '../../../providers/form.service';
import { ClientsService } from '../../../service/clients.service';
import { SwalService } from '../../../service/swal.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
export class InvoiceData {
  public invoice_data = {
    name: [],
    rfc: [],
    address: [],
    outdoor_number: [],
    inside_number: [],
    phone: [],
    email: [],
    between_streets: [],
    colony: [],
    postal_code: [],
    state: [],
    county: [],
  }

  constructor() { }
}

export class CardData {
  public cards = {
    name: [],
    address: [],
    outdoor_number: [],
    inside_number: [],
    phone: [],
    email: [],
    between_streets: [],
    colony: [],
    postal_code: [],
    state: [],
    county: [],
    country: [],
    card_number: [],
    exp_month: [],
    exp_year: [],
    default_card: []
  }

  constructor() { }
}
@Component({
  selector: 'app-client-edit',
  templateUrl: './client-edit.component.html',
  styleUrls: ['./client-edit.component.scss']
})
export class ClientEditComponent implements OnInit, OnDestroy {
  @Input() data: any;
  form: FormGroup = this.formBuilder.group({
    'name': ['', Validators.required],
    'rfc': [''],
    'address': ['', Validators.required],
    'outdoor_number': ['', Validators.required],
    'inside_number': [''],
    'phone': ['', Validators.required],
    'email': ['', Validators.required],
    'between_streets': ['', Validators.required],
    'colony': ['', Validators.required],
    'postal_code': ['', Validators.required],
    'state': ['', Validators.required],
    'county': ['', Validators.required],
    'coordinate': this.formBuilder.group({'latitude': ['', Validators.required], 'longitude': ['', Validators.required]}),
    'extra_data': [''],
    'cards': new FormArray([])
  });

  broadcast$: Subscription;
  constructor(
    private formBuilder: FormBuilder,
    private fromService: FromService,
    public activeModal: NgbActiveModal,
    private clientService: ClientsService,
    private swal: SwalService) { }

  ngOnInit() {
    this.clientService.show(this.data.id_clients).subscribe((data: any) => {
      let client = data.response;

      Object.keys(client).forEach(key => {
        if (['cards', 'invoice_data', 'coordinate.latitude', 'coordinate.longitude'].indexOf(key) == -1 && this.form.controls[key]) {
          this.form.controls[key].setValue(client[key]);
        }
      });

      const grp = [];
      const inD = new InvoiceData;
      Object.keys(inD.invoice_data).forEach((key) => {
        const value = (client.invoice_data !== null && key in client.invoice_data) ? client.invoice_data[key] : '';
        grp[key] = [value, inD.invoice_data[key]];
      });
      this.form.addControl('invoice_data', this.formBuilder.group(grp));

      const invoiceName = this.form.get('invoice_data').get('name');
      const invoiceRfc = this.form.get('invoice_data').get('rfc');
      this.form.get('invoice_data').valueChanges.subscribe(invoice_data => {
        if (invoice_data.name !== '' || invoice_data.name !== null || invoice_data.rfc !== '' || invoice_data.rfc !== null) {
          invoiceName.setValidators([Validators.required]);
          invoiceRfc.setValidators([Validators.required]);
        }
        invoiceName.updateValueAndValidity({onlySelf : true});
        invoiceRfc.updateValueAndValidity({onlySelf : true});
      });

      if (client.cards) {
        const cardData = this.form.get('cards') as FormArray;
        for (let i = 0; i < client.cards.length; i++) {
          const grp = [];
          const cardD = new CardData;
          let card = client.cards[i];
          Object.keys(cardD.cards).forEach((key) => {
            grp[key] = [card[key], cardD.cards[key]];
          });
          cardData.push(this.formBuilder.group(grp))
        }
      }
    });
    this.fromService.setForm(this.form);
  }

  ngOnDestroy(): void {
    this.broadcast$.unsubscribe();  
  }

  get cards(): FormArray { return this.form.get('cards') as FormArray; }

  updateClient() {
    if (this.form.valid) {
      const params = this.form.value;
      this.swal.actionConfirmation('¿Esta seguro de querer actualizar los datos del cliente?').then((result) => {
        if (result.value) {
          this.clientService.update(this.data.id_clients, params).subscribe((data: any) => {
            if (data.success) {
              this.swal.success().then(() => {
                this.activeModal.dismiss();
              });
            } else {
              this.swal.error('Ocurrio un error al actualizar los datos');
            }
          });
        }
      });
    }
  }
}
