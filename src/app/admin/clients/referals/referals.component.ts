import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ClientsService } from '../../../service/clients.service';
import { Subscription } from 'rxjs';
import { BroadcastService } from '../../../service/broadcast.service';

@Component({
  selector: 'app-referals',
  templateUrl: './referals.component.html',
  styleUrls: ['./referals.component.scss']
})
export class ReferalsComponent implements OnInit, OnDestroy {
  @Input() data: any;

  clientSub$: Subscription;
  broadcast$: Subscription;
  client: any;
  info: any;

  constructor(public activeModal: NgbActiveModal, private clientService: ClientsService, private broadcast: BroadcastService) { }

  ngOnInit() {
    this.info = this.data;
    this.broadcast$ = this.broadcast.events.subscribe(event => {
      switch(event.name) {
        case 'client-referal-show': this.info = event.data;
      }
    });
    this.clientSub$ = this.clientService.getHierarchicalTree(this.data.id_clients).subscribe((resp: any) => {
      this.client = resp.response;
    });
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.clientSub$.unsubscribe();
    this.broadcast$.unsubscribe();
  }

}
