import { Component, OnInit, Input } from '@angular/core';
import { isArray } from 'util';
import { BroadcastService } from '../../../../service/broadcast.service';

@Component({
  selector: 'app-clients-referals-treeview',
  templateUrl: './treeview.component.html',
  styleUrls: ['./treeview.component.scss']
})
export class TreeviewComponent implements OnInit {
  @Input() clients;

  parsedClients = [];
  
  constructor(private broadcast: BroadcastService) { }

  ngOnInit() {
    if (Array.isArray(this.clients)) {
      this.parsedClients = [...this.clients];
    } else {
      Object.keys(this.clients).forEach(key => {
        this.parsedClients.push(this.clients[key]);
      });
    }
  }

  getLength(clients?: any): number {
    let length = 0;
    if (clients) {
      length = Object.keys(clients).length;
    }
    return length;
  }

  showInfo(client) {
    this.broadcast.fire({
      name: 'client-referal-show', 
      data: client
    });
  }

}
