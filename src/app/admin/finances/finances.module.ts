import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { BadDebtsComponent } from './bad-debts/bad-debts.component';
import { FinancesRoutes } from './finances.routing';
import { FinancesComponent } from './finances.component';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(FinancesRoutes),
    ComponentsModule
  ],
  declarations: [
    FinancesComponent,
    BadDebtsComponent
  ],
})
export class FinancesModule { }
