import { Routes } from '@angular/router';
import { BadDebtsComponent } from './bad-debts/bad-debts.component';

export const FinancesRoutes: Routes = [
    {
        path: '',
        children: [
            {
                path: 'malas-deudas', component: BadDebtsComponent
            }
        ]
    }
]