import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ClientsService } from '../../../service/clients.service';
import { SwalService } from '../../../service/swal.service';
import { BroadcastService } from '../../../service/broadcast.service';
import { BadDebtsService } from '../../../service/bad-debts.service';

@Component({
  selector: 'app-bad-debts',
  templateUrl: './bad-debts.component.html',
  styleUrls: ['./bad-debts.component.scss']
})
export class BadDebtsComponent implements OnDestroy, OnInit {
  public dataTableConfig = {
    config: {
      base: this.badDebts,
      api: 'getBadDebtsClients'
    },
    columns: [
      {
        display: 'Nombre',
        field: 'name',
        type: 'text'
      },
      {
        display: 'Grupo de ventas',
        field: 'id_groups.name',
        type: 'text'
      },
      {
        display: 'Fecha de alta',
        field: 'created_at',
        type: 'date'
      },
      {
        display: 'Email',
        field: 'email',
        type: 'text'
      },
      {
        display: 'Deudas',
        field: 'debts',
        type: 'date'
      },
      {
        display: 'ACCIONES',
        field: '',
        type: 'actions',
        options: [
          {
            display: 'Restaurar al cliente',
            // icon: 'fa fa-file-text',
            event: 'client.restore',
            conditionality: 'true'
          }
        ]
      }
    ]
  };

  broadcast$: Subscription;
  constructor(
    private badDebts: BadDebtsService,
    private broadcast: BroadcastService,
    public swal: SwalService
  ) { }

  ngOnInit() {
    this.broadcast$ = this.broadcast.events.subscribe(event => {
      switch (event.name) {
        case 'client.restore': this.restoreClientItem(event);break;
      }
    });
  }

  ngOnDestroy(): void {
    this.broadcast$.unsubscribe();
  }
  
  restoreClientItem(data) {
    this.swal.actionConfirmation('¿Desea restaurar al cliente?').then((result) => {
      if (result.value) {
        this.badDebts.restoreClient(data.data.id_clients).subscribe((response: any) => {
          if (response.success) {
            this.swal.success();
          } else {
            this.swal.error(response.message);
          }
        });
      }
    });
  }
}
