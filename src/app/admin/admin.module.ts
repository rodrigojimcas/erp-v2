import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminRoutes } from './admin.routing';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ChartsModule } from 'ng2-charts';
import { ClientsModule } from './clients/clients.module';
import { TicketsModule } from './tickets/tickets.module';
import { ErrorCodesModule } from './error-codes/error-codes.module';
import { ComponentsModule } from '../components/components.module';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AuthorizationInterceptorService} from '../providers/authorization-interceptor.service'
import { CatchAllInterceptorService} from '../providers/catch-all-intercerptor.service';
import { FinancesModule } from './finances/finances.module';
import { AdministratorsModule } from './administrators/administrators.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ChartsModule,
    HttpClientModule,
    // BsDropdownModule,
    //ButtonsModule.forRoot(),
    ReactiveFormsModule,
    RouterModule.forChild(AdminRoutes),
    AdministratorsModule,
    ClientsModule,
    TicketsModule,
    ErrorCodesModule,
    FinancesModule,
    ComponentsModule
  ],
  declarations: [
    DashboardComponent,
  ],
  entryComponents: [
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthorizationInterceptorService,
      multi: true
    }
  ]
})

export class AdminModule { }