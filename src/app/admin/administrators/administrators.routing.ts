import {Routes} from '@angular/router';
import {AuthGuardService} from '../../service/auth-guard.service';
import { AdministratorsComponent } from './administrators.component';

export const AdministratorsRoutes: Routes = [
    {
        path: '',
        component: AdministratorsComponent
    },
    {
        path: '',
        children: [
            {path: 'administradores', component: AdministratorsComponent}
        ]
    }
]