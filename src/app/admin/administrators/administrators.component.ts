import { Component, OnInit, OnDestroy } from '@angular/core';
import { ModalComponent } from '../../components/modal/modal.component';
import { Subscription } from 'rxjs';
import { SharedComponent } from '../../model/shared-component';
import { SwalService } from '../../service/swal.service';
import { UsersService } from '../../service/users.service';
import { BroadcastService } from '../../service/broadcast.service';
import { AdministratorCreateEditComponent } from './administrator-create-edit/administrator-create-edit.component';

@Component({
  selector: 'app-administrators',
  templateUrl: './administrators.component.html',
  styleUrls: ['./administrators.component.scss']
})
export class AdministratorsComponent implements OnInit, OnDestroy {

  public dataTableConfig = {
    config: {
      base: this.users,
      api: 'getAdmins'
    },
    columns: [
      {
        display: 'Nombre',
        field: 'name',
        type: 'text'
      },
      {
        display: 'Email',
        field: 'email',
        type: 'text'
      },
      {
        display: 'Fecha de alta',
        field: 'created_at',
        type: 'date'
      },
      {
        display: 'ACCIONES',
        field: '',
        type: 'actions',
        options: [
          
          {
            display: 'Editar',
            icon: 'fa fa-pencil',
            event: 'user.userEdit',
            conditionality: 'true'
          },
          
        ]
      }
    ]
  };
  
  actionEventSubscription: Subscription;

  constructor(
    private users: UsersService,
    private broadcast: BroadcastService,
    public appModal: ModalComponent,
    public swal: SwalService,
  ) { }

  ngOnInit(): void {
    this.actionEventSubscription = this.broadcast.events.subscribe(event => {
      console.log('subscription data: ', event);
      switch (event.name) {
        case 'user.userEdit': this.userEditItem(event.data); break;
      }
    });
  }

  ngOnDestroy(): void {
    this.actionEventSubscription.unsubscribe();
  }

  userEditItem(data) {
    const props: SharedComponent = new SharedComponent(AdministratorCreateEditComponent, data, { title: `Editar datos de ${data.name || 'Administrador'}` });
    this.appModal.openXl(props);
    console.log(props)
  }

}
