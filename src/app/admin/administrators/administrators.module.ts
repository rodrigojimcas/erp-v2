import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { ComponentsModule } from '../../components/components.module';
import { AdministratorsComponent } from './administrators.component';
import { AdministratorsRoutes } from './administrators.routing';

@NgModule({
    imports: [
        RouterModule.forChild(AdministratorsRoutes),
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        ComponentsModule
    ],
    declarations: [
        AdministratorsComponent
    ],
    entryComponents: [],
    exports: [AdministratorsComponent],
    providers: [
        NgbCarouselConfig
    ]
})
export class AdministratorsModule { }
