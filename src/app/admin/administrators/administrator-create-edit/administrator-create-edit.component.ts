import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { FromService } from '../../../providers/form.service';
import { SwalService } from '../../../service/swal.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { UsersService } from '../../../service/users.service';

@Component({
  selector: 'app-administrator-create-edit',
  templateUrl: './administrator-create-edit.component.html',
  styleUrls: ['./administrator-create-edit.component.scss']
})
export class AdministratorCreateEditComponent implements OnInit, OnDestroy {
  @Input() data: any;
  form: FormGroup = this.formBuilder.group({
    'name': ['', Validators.required],
    'image': [''],
    'phone': ['', Validators.required],
    'email': ['', Validators.required],
    'contractor': [''],
    'status': [''],
    'type': [''],
    'team_id':['']
  })

  definition = {
    name: {
      type: 'text',
      label: 'Nombre Completo'
    },
    email: {
      type: 'email',
      label: 'Correo Electronico'
    },
    phone: {
      type: 'phone',
      label: 'Telefono'
    },
    image: {
      type: 'image',
    },
  }

  constructor(
    private formBuilder: FormBuilder,
    private fromService: FromService,
    public activeModal: NgbActiveModal,
    private service: UsersService,
    private swal: SwalService) { }

  ngOnInit() {
    this.service.show(this.data.id).subscribe((data: any) => {
      let admin = data.response || this.data;
      Object.keys(admin).forEach(key => {
        if (admin.hasOwnProperty(key) && !! this.form.controls[key]) {
          this.form.controls[key].setValue(admin[key]);
        }
      });
    });
    this.fromService.setForm(this.form);
  }

  ngOnDestroy(){

  }

}
