import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ClientsComponent } from './clients/clients.component';
import { ErrorCodesComponent } from './error-codes/error-codes.component';
import { FinancesComponent } from './finances/finances.component';
import { AuthGuardService } from '../service/auth-guard.service';
import { TicketsComponent } from './tickets/tickets.component';
import { ngModuleJitUrl } from '@angular/compiler';
import { NgModule } from '@angular/core';
import { AdministratorsComponent } from './administrators/administrators.component';

export const AdminRoutes: Routes = [
    {path: '', redirectTo: '/admin/inicio', pathMatch: 'full'},
    {path: 'inicio', redirectTo: '/admin/inicio', pathMatch: 'full'},
    {
        path: '',
        children: [
            { 
                path: 'inicio', component: DashboardComponent,
                canActivate: [AuthGuardService]
            },
            { path: '', 
                component: ClientsComponent,
                children:[{
                    path: 'clientes',
                    loadChildren: './clients/clients.module#ClientsModule'
                }]
            },
            { path: '', 
                component: AdministratorsComponent,
                children:[{
                    path: 'administradores',
                    loadChildren: './administrators/administrators.module#AdministratorsModule'
                }]
            },
            { path: '', 
            component: TicketsComponent,
            children:[{
                path: 'tickets',
                loadChildren: './tickets/tickets.module#TicketsModule'
            }]
          },
          { path: '', 
            component: FinancesComponent,
            children:[{
                path: 'finanzas',
                loadChildren: './finances/finances.module#FinancesModule'
            }]
          }
        ]
    },
    {
        path: '',
        children: [
            {path: 'codigos_de_error', component: ErrorCodesComponent}
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(AdminRoutes)
    ]
})

export class AdminModule {}