import {Type} from '@angular/core';

export class SharedComponent {
    constructor(public component: Type<any>, public data: any, public config?: any) {}
}
