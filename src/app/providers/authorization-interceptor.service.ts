import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router'

@Injectable({
  providedIn: 'root'
})

export class AuthorizationInterceptorService implements HttpInterceptor {

  constructor(
    private router: Router,
  ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const token: string = localStorage.getItem('access_token');
    let request = req;
    
    if (token) {
      request = req.clone({
        setHeaders: {
          Authorization: `Bearer ${token}`
        },
        withCredentials: true
      });
    }
    return next.handle(request)
    // .pipe(
    //   catchError((err: HttpErrorResponse) => {
    //     if (err.status === 401) {
    //       this.router.navigateByUrl('/login');
    //     }

    //     return throwError(err);
    //   })
    // )

  }
}