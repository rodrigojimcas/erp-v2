import { catchError, map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Router } from '@angular/router'
import Swal from 'sweetalert2';
import { SwalService } from '../service/swal.service';

@Injectable({
    providedIn: 'root'
})

export class CatchAllInterceptorService implements HttpInterceptor {

    constructor(
        private router: Router,
        public swal: SwalService
    ) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        return next.handle(req).pipe(
            catchError(error => {
                let errorMessage = '';
                if (error instanceof ErrorEvent) {
                    // client-side error
                    errorMessage = `Client-side error: ${error.error.message}`;
                    this.swal.error(errorMessage);
                } else {
                    // backend error
                    errorMessage = `Server-side error: ${error.status} ${error.message}`;
                    console.log(error)
                    switch (error.status) {
                        case 401:
                            this.router.navigateByUrl('/login');
                            this.swal.warning('!Sesión expirada!');
                            break;
                        default:
                            this.swal.error(errorMessage);
                            break;
                    }
                    // if (error.status === 401) {
                    //     this.router.navigateByUrl('/login');
                    //     this.swal.warning('!Sesión expirada!');
                    // }
                }

                // aquí podrías agregar código que muestre el error en alguna parte fija de la pantalla.
                // this.errorService.show(errorMessage);
                // console.warn(errorMessage);
                return throwError(errorMessage);
            })
        );
    }
}