import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { AuthorizationService } from '../../service/authorization.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UsersService } from '../../service/users.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [AuthorizationService]
})
export class LoginComponent {
  loginForm: FormGroup;
  email: string;
  password: string;
  errors = [];
  loading = false;

  constructor(private router: Router,
    private formBuilder: FormBuilder,
    private authorizationService: AuthorizationService,
    private userService: UsersService,
  ) {

    this.loginForm = this.formBuilder.group({
      email: ['luis@aguagente.com', Validators.required],
      password: ['admin001', Validators.required],
      appVersion: ['']
    });

    const access_token = localStorage.getItem('access_token');
    const user = localStorage.getItem('user');


    if (access_token != null && user != null) {
      this.loading = true;
      const userData = JSON.parse(user);
      // console.log('usuario: ', userData);
      // this.userService.user(userData.id).subscribe((data: any) => {
      //   if (data.errorMessage !== null) {
      //     localStorage.removeItem('access_token');
      //     localStorage.removeItem('user');
      //     this.errors.push('Su sesión Finalizo');
      //     this.loading = false;
      //   } else {
      //     this.router.navigate(['admin/inicio']);
      //   }
      // });
    }

  }

  logIn() {
    this.errors = [];

    if (this.loginForm.valid) {

      this.loading = true;
      const formData = this.loginForm.value;

      this.authorizationService.login(this.loginForm.value).subscribe((data: any) => {
        localStorage.setItem('access_token', data.access_token);
        localStorage.setItem('user', JSON.stringify(data.user));
        // this.toastr.success('!Has ingresado exitosamente!');
        this.router.navigate(['/admin/inicio']);
        // if (data.code === 200) {
        //   console.log('Exito');
        //   console.log(data.response);
        //   localStorage.setItem('access_token', data.response.access_token);
        //   localStorage.setItem('user', JSON.stringify(data.response.user));
        //   // this.toastr.success('!Has ingresado exitosamente!');
        //   this.router.navigate(['/admin/inicio']);
        // } else if (data.code === 0) {
        //   this.loading = false;
        //   this.errors = ['Error en el servidor'];
        // } else {
        //   this.loading = false;
        //   // this.toastr.error('!Credenciales invalidas!');
        //   this.errors = data.errorMessage.error.user_authentication;
        // }
      });
    }
  }

}
